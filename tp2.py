class Box:
    def __init__(self):
        self._contents = []
        self._ouvert = False
        self._capacite = None
    
    def has_room_for(self, thing):
       if not self._capacite:
           return True
       elif self._capacite > thing.volume():
           return True
       else:
           return False

    def action_add(self, thing):
        if self.has_room_for(thing) and self.is_open():
            self.add(thing)
            if self._capacite:
                self._capacite -= thing.volume()
            return True
        else:
            return False


    def set_capacity(self, cap):
        self._capacite = cap

    def capacity(self):
        return self._capacite

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, machin):
        return machin in self._contents

    def remove(self, truc):
        self._contents.remove(truc)

    def is_open(self):
        return self._ouvert

    def close(self):
        self._ouvert = False
    
    def open(self):
        self._ouvert = True

    def action_look(self):
        if not self.is_open():
            return "la boîte est fermée !"
        else:
            return "la boite contient : "+', '.join(self._contents)
class Thing:
    def __init__(self,volume):
        self._volume = volume

    def volume(self):
        return self._volume
        
    
